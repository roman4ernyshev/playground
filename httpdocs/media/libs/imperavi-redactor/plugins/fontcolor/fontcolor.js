if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.fontcolor = function()
	{
		return {
			init: function()
			{
				//var colors = [
				//	'#ffffff', '#000000', '#eeece1', '#1f497d', '#4f81bd', '#c0504d', '#9bbb59', '#8064a2', '#4bacc6', '#f79646', '#ffff00',
				//	'#f2f2f2', '#7f7f7f', '#ddd9c3', '#c6d9f0', '#dbe5f1', '#f2dcdb', '#ebf1dd', '#e5e0ec', '#dbeef3', '#fdeada', '#fff2ca',
				//	'#d8d8d8', '#595959', '#c4bd97', '#8db3e2', '#b8cce4', '#e5b9b7', '#d7e3bc', '#ccc1d9', '#b7dde8', '#fbd5b5', '#ffe694',
				//	'#bfbfbf', '#3f3f3f', '#938953', '#548dd4', '#95b3d7', '#d99694', '#c3d69b', '#b2a2c7', '#b7dde8', '#fac08f', '#f2c314',
				//	'#a5a5a5', '#262626', '#494429', '#17365d', '#366092', '#953734', '#76923c', '#5f497a', '#92cddc', '#e36c09', '#c09100',
				//	'#7f7f7f', '#0c0c0c', '#1d1b10', '#0f243e', '#244061', '#632423', '#4f6128', '#3f3151', '#31859b',  '#974806', '#7f6000'
				//];

				var colors = [//#424242
					'#000000','#424242','#666666','#999999','#b7b7b7','#cccccc','#d9d9d9','#efefef','#f3f3f3','#ffffff',
					'#94140b','#f5281b','#fb9828','#fef938','#2af72e','#2dfcfe','#4d89e3','#103df6','#9843fb','#fc4cfc',
					'#e4b9b1','#f3cdcd','#fce4cf','#fff2cf','#d7ebd1','#d0e1e3','#cadaf7','#d3e0f2','#dad3e8','#efcfdc',
					'#db7e6f','#e89a9b','#fcc89f','#fce59d','#b2daa8','#a5c3c8','#a5c3f2','#a3c6e2','#b3a8d3','#d6a6c0',
					'#c8422d','#d96966','#f4b16f','#fdd772','#93c27e','#75a6ae','#6ea0ed','#72a8db','#8d7ec2','#be7da0',
					'#a21f0f','#cb1f14','#e89041','#efc244','#71a454','#47828d','#407ad6','#4187c6','#6751a6','#a54e78',
					'#832113','#9a150f','#b65d19','#bd9020','#3c7423','#16505e','#1759c4','#135593','#362176','#731d48',
					'#5a1005','#660a06','#753f0f','#7f5e12','#284e19','#0f3537','#214585','#0b385f','#201648','#4c1231'
				];

				//var buttons = ['fontcolor', 'backcolor'];
				var buttons = ['fontcolor'];

				for (var i = 0; i < buttons.length; i++)
				{
					var name = buttons[i];

					var button = this.button.addAfter('alignment', name, this.lang.get(name));
					var $dropdown = this.button.addDropdown(button);

					//$dropdown.width(242);
					$dropdown.width(220);
					this.fontcolor.buildPicker($dropdown, name, colors);

				}
			},
			buildPicker: function($dropdown, name, colors)
			{
				var rule = (name == 'backcolor') ? 'background-color' : 'color';

				var len = colors.length;
				var self = this;
				var func = function(e)
				{
					e.preventDefault();
					self.fontcolor.set($(this).data('rule'), $(this).attr('rel'));
				};

				for (var z = 0; z < len; z++)
				{
					var color = colors[z];

					var $swatch = $('<a rel="' + color + '" data-rule="' + rule +'" href="#" style="float: left; font-size: 0; border: 2px solid #fff; padding: 0; margin: 0; width: 22px; height: 22px;"></a>');
					$swatch.css('background-color', color);
					$swatch.on('click', func);

					$dropdown.append($swatch);
				}

				var $elNone = $('<a href="#" style="display: block; clear: both; padding: 5px; font-size: 12px; line-height: 1;"></a>').html(this.lang.get('none'));
				$elNone.on('click', $.proxy(function(e)
				{
					e.preventDefault();
					this.fontcolor.remove(rule);

				}, this));

				$dropdown.append($elNone);
			},
			set: function(rule, type)
			{
				this.inline.removeStyleRule(rule);
				this.inline.format('span', 'style', rule + ': ' + type + ';');
			},
			remove: function(rule)
			{
				this.inline.removeStyleRule(rule);
			}
		};
	};
})(jQuery);
