function AppSite() {

	this.options = {}

	var self = this;

	this.initSignup = function(options)
	{
		appUtils.extend(self.options, options);
		appUtils.addFingerPrint('register-form', 'Users[fpToken]');
		appUtils.addFingerPrint2('register-form', 'Users[fpToken2]');
	};

	this.initLogin = function()
	{
		self.loginBaseInit('login-form');

		if ($('#facebook-login-button').length>0) {
			appUtils.addFingerPrint('facebook-login-button', 'data-fptoken');
			appUtils.addFingerPrint2('facebook-login-button', 'data-fptoken2');
		}
	};


	this.loginBaseInit = function(id)
	{
		appUtils.addFingerPrint(id, 'LoginForm[fpToken]');
		appUtils.addFingerPrint2(id, 'LoginForm[fpToken2]');
	};

	this.initRestore = function()
	{
		appUtils.addFingerPrint('restore-form', 'Users[fpToken]');
		appUtils.addFingerPrint2('restore-form', 'Users[fpToken2]');
	}
}
var appSite = new AppSite();