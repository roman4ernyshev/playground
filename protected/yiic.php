<?php
if (!empty($GLOBALS["argv"]) AND in_array('--local', $GLOBALS["argv"])) {
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',0);
}
else {
	defined('YII_DEBUG') or define('YII_DEBUG',false);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
}
$config=dirname(__FILE__).'/config/console.php';

// fix for fcgi
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));

require_once(dirname(__FILE__).'/vendor/autoload.php');

require_once(dirname(__FILE__).'/../framework/yii.php');

Yii::$enableIncludePath = false;

require_once(dirname(__FILE__).'/../protected/components/ConsoleApplication.php');
$app = Yii::createApplication('ConsoleApplication', $config);

$app->commandRunner->addCommands(YII_PATH.'/cli/commands');

$env=@getenv('YII_CONSOLE_COMMANDS');
if(!empty($env))
	$app->commandRunner->addCommands($env);

Yii::setPathOfAlias('webroot', realpath(dirname(__FILE__).'/../httpdocs/'));
Yii::setPathOfAlias('files', realpath(dirname(__FILE__).'/../files/'));
$app->run();