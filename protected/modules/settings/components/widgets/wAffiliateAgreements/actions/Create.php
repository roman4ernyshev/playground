<?php
class Create extends WidgetBaseAction
{
	public function run()
	{
		$model = new AffiliateAgreements('create-save');

		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];

			if (!$model->validate())
				$this->controller->jsonResponse(['error'=>MyUtils::getFirstError($model)]);

			// Бизнес логика спрятана в метод. В экшне ее не должно быть, потому что
			// существует еще один экшн, который делает тоже самое (в income proposals list модуля solos).
			$model->makeAgr(Yii::app()->user->id, $model->profileUserModel->id);

			$widget = $this->getWidgetInstance();
			$this->controller->jsonResponse([
				'callback'=>[
					'appMain.showToast("New affiliate agreement created", "success")',
					'settingsWidgetAgreements.init()',
				],
				'container' => '#'.$widget->containerId,
				'content' => $widget->run(),
			]);
		}

		$this->controller->layoutContainer = '#new-agr-container';
		$this->controller->render('application.modules.settings.components.widgets.wAffiliateAgreements.views.partial.create', ['model'=>$model]);
	}
}