function SettingsGeneral() {

	this.options = {}

	var self = this;

	this.initSound = function()
	{
		$('.sound-notification-item-checkbox').bind('change', function() {
			$('#sound-notification-submit').click();
		});
	}

	this.initOpenAfterLogin = function()
	{
		$('.js-oal').on('change', '.js-oal-autosubmit', function(e) {
			$('.js-oal-submit').click();
		});
	}

	this.initMyProfileLink = function()
	{
		$('.js-mpl').on('click', '.js-path-toggler', function(e) {
			$('.js-path-static').addClass('hidden');
			$('.js-path-dynam').removeClass('hidden');
			$('.js-link-submit').removeClass('hidden');
			$('.js-path-toggler').addClass('hidden');
			e.preventDefault();
		});
	}

	this.initLng = function()
	{
		$('.js-lng').on('change', '.js-lng-autosubmit', function(e) {
			$('.js-lng-submit').click();
		});
	}
}
var settingsGeneral = new SettingsGeneral();