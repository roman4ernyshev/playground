<?php

class MatchMask extends CApplicationComponent
{
	public $namedMasks = [];

	public function check($arg=false)
	{
		$isMatchMask = false;

		if (is_array($arg)) {
			$mask = $arg;
		} else {
			if (empty($this->namedMasks[$arg])) {
				return false;
			} else {
				$mask = $this->namedMasks[$arg];
			}
		}

		if (!($routeCurrent = $this->_getRoute())) {
			return false;
		}

		$inputGetParams = isset($_GET) ? $_GET : array();

		foreach ($mask['include'] as $key=>$val) {
			if (is_integer($key)) {
				$routeMask = $val;
				$params = array();
			} else {
				$routeMask = $key;
				$params = $val;
			}

			$routeMask = explode('.', $routeMask);
			if (sizeof($routeMask) != 3) {
				throw new CException('Invalid route lenght in params');
			}
			$isAction = $routeCurrent[2]==$routeMask[2] || $routeMask[2]=='*';
			$isController = $routeCurrent[1]==$routeMask[1] || $routeMask[1]=='*';
			$isModule = $routeCurrent[0]==$routeMask[0] || $routeMask[0]=='*';

			if (!empty($params)) {
				$isParams = array_diff_assoc($params, $inputGetParams) ? false : true;
			} else {
				$isParams = true;
			}

			if ($isAction && $isController && $isModule && $isParams) {
				$isMatchMask = true;
			}
		}
		if ($isMatchMask && !empty($mask['exclude'])) {
			foreach ($mask['exclude'] as $key=>$val) {
				if (is_integer($key)) {
					$routeMask = $val;
					$params = array();
				} else {
					$routeMask = $key;
					$params = $val;
				}

				$routeMask = explode('.', $routeMask);
				$isAction = $routeCurrent[2]==$routeMask[2] || $routeMask[2]=='*';
				$isController = $routeCurrent[1]==$routeMask[1] || $routeMask[1]=='*';
				$isModule = $routeCurrent[0]==$routeMask[0] || $routeMask[0]=='*';

				if (!empty($params)) {
					$isParams = array_diff_assoc($params, $inputGetParams) ? false : true;
				} else {
					$isParams = true;
				}

				if ($isAction && $isController && $isModule && $isParams) {
					$isMatchMask = false;
				}
			}
		}
		return $isMatchMask;
	}

	public function isMatchMasksList()
	{
		$isMatchMasks = [];
		foreach ($this->namedMasks as $mName=>$mVal) {
			$isMatchMasks[$mName] = $this->check($mName) ? 1 : 0;
		}
		return $isMatchMasks;
	}

	private function _getRoute()
	{
		if (Yii::app()->request->isModal()) {
			$originalRoute = Yii::app()->request->getOriginalRoute();
			if (empty($originalRoute)) {
				throw new CException('Route is empty');
			}
			$arr = explode('/', $originalRoute);
			if (sizeof($arr)==2) {
				$moduleId = 'app';
				$controllerId = $arr[0];
				$actionId = $arr[1];
			} elseif (sizeof($arr)==3) {
				$moduleId = $arr[0];
				$controllerId = $arr[1];
				$actionId = $arr[2];
			} else {
				throw new CException('Route has wrong format');
			}
		} else {
			$controller = Yii::app()->getController();
			$controllerId = $controller->id;
			if (!$controller->action) {
				return false;
			}
			$actionId = $controller->action->id;
			$moduleId = $controller->getModule() ? $controller->getModule()->id : 'app';
		}
		return [$moduleId, $controllerId, $actionId];
	}
}