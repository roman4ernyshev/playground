<?php
class WebApplication extends CWebApplication
{
	public function createAbsoluteUrlSch($route, $params=[])
	{
		$url = $this->createAbsoluteUrl($route, $params, 'http');
		return str_replace('http:', '', $url);
	}

	public function createUrl($route,$params=array(),$ampersand='&')
	{
		if (isset($params['noautologin'])) {
			$noautologin = $params['noautologin'];
		} elseif (isset($_GET['noautologin'])) {
			$noautologin = $_GET['noautologin'];
		}
		if (isset($noautologin) && $noautologin) {
			$params['noautologin'] = 1;
		}
		return parent::createUrl($route,$params,$ampersand);
	}
}