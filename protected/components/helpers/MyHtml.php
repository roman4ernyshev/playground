<?php

class MyHtml extends CHtml {

	/**
	 * Generates a push button that can submit the current form in POST method.
	 * @param string $label the button label
	 * @param mixed $url the URL for the AJAX request. If empty, it is assumed to be the current URL. See {@link normalizeUrl} for more details.
	 * @param array $ajaxOptions AJAX options (see {@link ajax})
	 * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
	 * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
	 * @return string the generated button
	 */
	public static function ajaxSubmitButton($label,$url,$ajaxOptions=array(),$htmlOptions=array())
	{
		$htmlOptions['type']='submit';
		return self::ajaxButton($label,$url,$ajaxOptions,$htmlOptions);
	}
	/**
	 * Generates a push button that can initiate AJAX requests.
	 * @param string $label the button label
	 * @param mixed $url the URL for the AJAX request. If empty, it is assumed to be the current URL. See {@link normalizeUrl} for more details.
	 * @param array $ajaxOptions AJAX options (see {@link ajax})
	 * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
	 * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
	 * @return string the generated button
	 */
	public static function ajaxButton($label,$url,$ajaxOptions=array(),$htmlOptions=array())
	{
		$ajaxOptions['url']=$url;
		$htmlOptions['ajax']=$ajaxOptions;
		return self::button($label,$htmlOptions);
	}
	/**
	 * Generates a button.
	 * @param string $label the button label
	 * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
	 * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
	 * @return string the generated button tag
	 * @see clientChange
	 */
	public static function button($label='button',$htmlOptions=array())
	{
		if(!isset($htmlOptions['name']))
		{
			if(!array_key_exists('name',$htmlOptions))
				$htmlOptions['name']=self::ID_PREFIX.self::$count++;
		}
		if(!isset($htmlOptions['type']))
			$htmlOptions['type']='button';
		if(!isset($htmlOptions['value']))
			$htmlOptions['value']=$label;

		if (!isset($htmlOptions['ajax']['type'])) {
			$htmlOptions['ajax']['type'] = 'POST';
		}
		if (!isset($htmlOptions['ajax']['context'])) {
			$htmlOptions['ajax']['context'] = 'js:this';
		}
		if (!isset($htmlOptions['ajax']['beforeSend'])) {
			$htmlOptions['ajax']['beforeSend'] = 'js:appMain.btnAjaxBeforeSend';
		}
		if (!isset($htmlOptions['ajax']['error'])) {
			$htmlOptions['ajax']['error'] = 'js:appMain.ajaxError';
		}

		parent::clientChange('click',$htmlOptions);
		return parent::tag('button',$htmlOptions, $label);
	}
}