<?php

class ConsoleCommand extends CConsoleCommand
{
	public $verbose = false;

	public function init()
	{
		parent::init();
		if ($this->_isOneInstanceAlreadyExist(pathinfo(__FILE__))) {
			if ($this->verbose) {
				echo "Already running process detected. Bye!\n";
			}
			exit;
		}
	}

	protected function isLocal()
	{
		return in_array('--local', $GLOBALS["argv"]);
	}

	protected function _isOneInstanceAlreadyExist($pathParts)
	{
		if (strpos(strtolower(php_uname('s')), 'windows') !== false) {
			return false;
		}

		$name = is_array($pathParts) ? $pathParts['basename'] : $pathParts;
		exec('ps aux | grep -i "'.$name.'" | grep -v grep', $result);
		if ($result && count($result) > 1) {
			return true;
		}
		return false;
	}
}