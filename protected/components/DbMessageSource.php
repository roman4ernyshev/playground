<?php

class DbMessageSource extends CDbMessageSource
{
	public static function resetCache()
	{
		foreach (Yii::app()->params['translatedLanguages'] as $language=>$val) {
			Yii::app()->cache->delete(CDbMessageSource::CACHE_KEY_PREFIX.'.messages.app.'.$language);
		}
	}
}